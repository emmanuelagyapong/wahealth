from django.db import models

# Create your models here.
class email_subscription(models.Model):
    email = models.EmailField(max_length=255)

    class Meta:
        db_table = 'email_subscription'



class email_recieved(models.Model):
    name = models.CharField(max_length=255)
    subject = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    message = models.TextField(max_length=700)


    class Meta:
        db_table = 'email_recieved'

