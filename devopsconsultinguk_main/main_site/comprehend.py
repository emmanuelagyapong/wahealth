import os 
import boto3
import pprint
import json
import textract

def handle_uploaded_file(title, recievedfile):
    print('the working directory is', os.getcwd())
    uploadPath = title
    with open(uploadPath.format(titleup=title), "wb") as destination:
        for chunk in recievedfile.chunks():
            destination.write(chunk)
    print('successfully uploaded pdf')
    text = textract.process(f"/usr/src/devopsconsultinguk_main/{title}")
 
    return detect_medical_entities(str(text))


def detect_medical_entities(text):
    scannedset = {"first"}
    print("started the scanning herer")
    client = boto3.client("comprehendmedical")
    client = boto3.client("comprehendmedical", region_name="eu-west-2")
    print("the text type is :", type(text))
    response = client.detect_entities_v2(Text=text)
    print('gets here for scan')
    entities = response['Entities']
    itemdict = {'medication':'dosage'}
    for entity in entities:
        print("started scan")
        if entity["Category"] == "MEDICATION":
            #print(entity["Category"], ":", entity["Text"],", " "dosage =", entity["Attributes"][0]["Text"])
            scannedset.add(entity["Category"])
            scannedset.add(entity["Text"])
            scannedset.add(entity["Attributes"][0]["Text"])
            Dosage = entity["Attributes"][0]["Text"]
            medication = entity["Text"]
            itemdict[medication]=Dosage

       
        if entity["Id"] == 89:
            print('name is: ', entity["Text"])
            scannedset.add(entity["Text"])
            PatientName = entity["Text"]


    print("this finished item is: ", itemdict)  
    return PatientName, itemdict
           
      
      