from multiprocessing import context
from django.shortcuts import render
from django.contrib import messages
from .models import email_subscription, email_recieved
from .email_logic import emailLogic
from django.http import HttpResponseRedirect
from .forms import  UploadFileForm
from .comprehend import handle_uploaded_file
import time

def index(request):
    return render(request, 'arsha/index.html')


def document_scan(request):
    print('gets to this bit')
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            title = form.data['title']
            print('form is valid')
            print(request.FILES["file"])
            PatientName, itemdict = handle_uploaded_file(title, request.FILES["file"])
            print('at view is :', PatientName, itemdict)
            time.sleep(4)
            keys = list(itemdict.keys())
            values = list(itemdict.values())
            third_key = keys[2]
            third_value = values[2]

            fourth_key = keys[3]
            fourth_value = values[3]

            fifth_key = keys[4]
            fifth_value = values[4]
            sending = {'PatientName': PatientName,
                       'third_key': third_key, 'third_value': third_value,
                        "fourth_key": fourth_key,  "fourth_value":fourth_value,
                         "fifth_key":fifth_key, "fifth_value": fifth_value}
                        


            return render(request, 'arsha/scannedpage.html', sending)
        else:
            print(form.errors)
            print('form not valid')
            form = UploadFileForm()  
        
    else: 
        print("not post")

    return HttpResponseRedirect('/')

def email_subscription_saver(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        email_subscription.objects.create(email=email)
    else:
        print("not posted")
    
    #return render(request, 'arsha/subscribed.html')
    return HttpResponseRedirect('/')




# Create your views here.
