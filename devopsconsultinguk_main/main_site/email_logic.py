import boto3
from botocore.exceptions import ClientError

class emailLogic:
    def __init__(self,reciever_email=None, name=None, subject=None, message=None):
        try:
            client = boto3.client('ses', region_name='eu-west-1')

            response = client.send_email(
                Source='help@devopsconsultinguk.co.uk',
                Destination={
                    'ToAddresses': [
                        'help@devopsconsultinguk.co.uk'
                    ]
                
                },
                Message={
                    'Subject': {
                        'Data': name +' with subject: ' + subject
                    },
                    'Body': {
                        'Text' : {
                            'Data': name + ' with email: ' + reciever_email + ' left message: '+ message
                        }
                    }
                }
            )
        except ClientError as e:
            print(e.response['Error']['Message'])
