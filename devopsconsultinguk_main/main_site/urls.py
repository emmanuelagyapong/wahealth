from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('documentscan/', views.document_scan, name='document_scan'),
    path('subscribed/', views.email_subscription_saver, name='email_subscription_saver')
]